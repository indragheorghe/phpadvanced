<?php
abstract class Foo
{
    static function bar()
    {
        echo "test\n";
    }
    abstract function Print_Text();
}

class Test_Mostenire extends Foo
{
    const textAfisat = 'Sunt o clasa de test';
    
    public function Print_Text() {
        printf("This is what we print: %s \n", self::textAfisat );
    }
}

//$obj = new Test_Mostenire;
#$obj->bar();

Foo::bar();
