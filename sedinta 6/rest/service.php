<?php

class Service{

	protected $status;
	protected $message;
	protected $data;
	private $db;

	public function __construct(){

	}

	public function deliver_response($message, $status,  $data, $type=NULL){

		header("HTTP/1.1 $status $message");

		$response['status']= $status;
		$response['message'] = $message;
		$response['data'] =  $data;

		echo json_encode($response);
	}

	public function get_book($request){
		$sql = "Select * from book where title like '%$request%'";
		$book = $this->database_connect()
		     ->query($sql)
		     ->fetchAll();

		return $book;
	}

	public function database_connect(){
		if (isset($this->db)) {
			return $this->db;
		} else {
			$this->db = new PDO('mysql:host=localhost;dbname=bookkeeper', 'root', '');
			return $this->db;
		}
	}
}