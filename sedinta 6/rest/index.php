<?php

// process client request -> URL (localhost/service/request=something)
// rasunsul e in json/xml
// URL trebuie sa fie user friendly

header('Content-Type:application/json');

include_once 'service.php';

$service = new Service();

if(!empty($_GET['request'])){
// we will use the bookkeeper database to return the data on a book for a certain book 	

	$request = $_GET['request'];
	$book = $service->get_book($request);

	if(empty($book)){
		$service->deliver_response("book not found", "404", NULL);
	}else{
		$service->deliver_response("book found", "200", $book);
	}
}else{
	$service->deliver_response("invalid request", "400", NULL);
}