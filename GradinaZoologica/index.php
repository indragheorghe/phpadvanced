<?php

include_once "Animale.php";

class Elefant extends AnimaleAbstract {

    private $specie = "mamifer"; // amfibian, mamifer, pasare
    private $culoareBlana = NULL;
    public $numeAnimal;
    public $tipMancare;

    function __construct($numeAnimal = 'Gigel') {
        $this->numeAnimal = $numeAnimal;
    }

    function Mananca($tipMancare) {
        $this->tipMancare = $tipMancare;
    }

    function Vorbeste($tipa) {
        printf("Acest animal face sunetul %s :", $tipa);
    }

    function Moare() {
        //parent::Moare();
        echo "Se reincarneaza";
    }

    public function setGreutate($greutate) {
        $this->greutate = $greutate;
    }

    public function getGreutate() {
        echo $this->greutate;
    }

    public function getSpecie() {
        echo $this->specie;
    }

    public function getCuloare_Blana() {
        echo $this->culoareBlana;
    }
    
    function Are_Trainer($bool=TRUE) {
        ;
    }
}

$elefant = new Elefant("Jumbo");
$elefant->setGreutate("1 tona");
$elefant->getGreutate();
echo "\nNumele elefantului este: " . $elefant->numeAnimal;
