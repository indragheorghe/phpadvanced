<?php
/**
 * Descrie clasa de baza pentru un animal
 */

abstract class AnimaleAbstract{
    
    private $greutate;
    private $specie; // amfibian, mamifer, pasare
    private $culoareBlana;
    private $numeAnimal;
    
    abstract function Mananca($tipMancare);
    abstract function Vorbeste($sunet);
    abstract function Are_Trainer($bool);
            function Moare(){
        $this->numeAnimal=NULL;
    }
    
}