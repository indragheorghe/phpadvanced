<?php

/**
 * Clasa care calculeaza aria cercului
 */
class Aria_Cercului 
{

    const PI = 3.14;
    private $raza;
            
    function __construct($raza) {
        $this->raza = $raza;
    }

    public function getArea() 
    {
        return self::PI * pow($this->raza, 2);
    }
}

$cerc = new Aria_Cercului(3);
echo $cerc->getArea();