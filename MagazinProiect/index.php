<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Magazin @BitAcademy</title>
    </head>
    <body>
        <?php
        include_once './Date.php';
        include_once './Produse.php';
        foreach ($Produse as $key => $value) {
            $produsObiect = new Produse(
                    $value['nume'], 
                    $value['categorie'], 
                    $value['pret']
            );
            $produsObiect->Afisaza_produs();
        }
        ?>
    </body>
</html>
