<?php

/**
 * Acesta clasa se ocupa de produse
 */
class Produse 
{

    //private $id = NULL;
    //private $cod = NULL;
    public $nume;
    public $descriere;
    public $pret;
    public $stoc;
    public $caracteristici = array();
    public $categorie;
    //private $codDeBare;

    function __construct($nume, $categorie, $pret) 
    {
        $this->nume=$nume;
        $this->pret=$pret;
        $this->categorie=$categorie;
    }

    function Afisaza_produs() 
    {
        echo "<div> <span> $this->nume </span>" .
        "<span> <a href='#'> $this->categorie</a> </span>" .
        "<span> $this->pret </span>"
        . "</div><br/>";
    }

}
