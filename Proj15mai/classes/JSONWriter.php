<?php

namespace Produs {
    include_once 'ProdusScrie.php';

    class JSON_Writer implements \Produs\classes_Produs_Scrie
    {

        public function Scrie(Produs $prod)
        {
            return json_encode(array('produs' => $prod));
        }

    }

}

