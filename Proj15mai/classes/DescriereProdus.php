<?php
namespace Produs {

    class Produs
    {

        public $nume;
        public $dataAchizitie;
        public $categorie;
        public $producator;

        function __construct(array $produs)
        {
            $this->nume = $produs['nume'];
            $this->dataAchizitie = $produs['dataAchizitie'];
            $this->categorie = $produs['categorie'];
            $this->producator = $produs['producator'];
        }

        public function Scrie_Produs(\Produs\classes_Produs_Scrie $writer)
        {
            return $writer->Scrie($this);
        }

    }

}
