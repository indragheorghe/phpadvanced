<?php

namespace factory {

    class Type_Factory
    {

        public static function Preia_Clasa_Tip($tip, $namespace)
        {
            $class = "$namespace\\" .$tip . '_Writer';

            if (class_exists($class)) {
                return new $class;
            } else {
                throw new \Exception("nu exista");
            }
        }

    }

}