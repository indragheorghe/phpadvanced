<?php

namespace Produs {
    include_once 'ProdusScrie.php';

    class XML_Writer implements \Produs\classes_Produs_Scrie
    {
        public function Scrie(Produs $prod)
        {
            $return = '<produs>';
            $return .= '<nume>'.$prod->nume.'</nume>';
            $return .= '<categorie>'. $prod->categorie.'</categorie>';
            $return .= '<dataAchizitie>'. $prod->dataAchizitie.'</dataAchizitie>';
            $return .= '<producator>'. $prod->producator.'</producator>';
            $return .= '</produs>';
            
            echo $return;
        }
    }

}