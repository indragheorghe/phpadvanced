<?php
namespace Produs;
include_once './classes/DescriereProdus.php';
include_once './classes/JSONWriter.php';
include_once './classes/XMLWriter.php';
include_once './classes/Factory.php';

$produs = [
    'categorie' => 'electrocasnice',
    'nume' => 'Masina de spalat',
    'dataAchizitie' => date('D-M-Y H:i'),
    'producator' => 'indesit'
];

$prod= new \Produs\Produs($produs);
$tip = 'JSON';

try{
    $writer = \factory\Type_Factory::Preia_Clasa_Tip($tip, __NAMESPACE__);
} catch (Exception $ex) {
    $writer = new \Produs\XML_Writer(); 
}

$result= $prod->Scrie_Produs($writer);
print_r($result."\n");

$decoded  = json_decode($result);
var_dump($decoded);
