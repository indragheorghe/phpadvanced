/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

});

$('#search_button').on("click", function () {
    var $var = $('#animal_name').val();
    $('#return_statement').text('');
    getNames($var);
});

function getNames($var)
{
    $.ajax({
        type: "POST",
        url: 'Response_Page.php',
        data: {
            data: $var
        },
        async: true,
        dataType: 'json'
    }).done(function (data) {
        if (data !== 'FALSE') {
            for (var p in data) {
                $str = '<p> The animal name is: ' + data[p].animal_name + ' and he belongs to this type: ' + data[p].animal_type + '</p><br/>';
                $('#return_statement').append($str);
            }
        }
        else{
            $('#return_statement').append('There is no '+ $var);
        }
    });
}
