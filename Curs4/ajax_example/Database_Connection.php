<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBConn {

    static private $_db = null; // The same PDO will persist from one call to the next

    private function __construct() {
        
    }

// disallow calling the class via new DBConn

    private function __clone() {
        
    }

// disallow cloning the class

    /**
     * Establishes a PDO connection if one doesn't exist,
     * or simply returns the already existing connection.
     * @return PDO A working PDO connection
     */
    public static function getConnection() {
        if (self::$_db == null) {
            try {
                self::$_db = new PDO('mysql:host=localhost;dbname=animals', 'root', '');
            } catch (PDOException $e) {
                die('<h1>Sorry. The Database connection is temporarily unavailable.</h1>');
            }
            return self::$_db;
        } else {
            return self::$_db;
        }
    }

}

