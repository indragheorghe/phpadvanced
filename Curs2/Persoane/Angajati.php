<?php

namespace Angajati {
    include_once 'Persoane.php';

    class Angajati extends \Persoane\Persoana
    {

        private $name;
        public $var1;
        public $var2;

        function __construct()
        {
            
        }

        function __destruct()
        {
            echo 'obiectul a fost aruncat la gunoi ' . "\n";
        }

        public function __call($name, $arguments)
        {
            if ($name == 'test' && count($arguments) < 1) {
                $this->test();
            } else {
                echo $name . ' does not exist for these arguments ' . implode($arguments) . "\n";
            }
        }

        public function __get($name)
        {
            if (isset($this->$name)) {
                return $this->$name;
            } else {
                return "missing property \n";
            }
        }

        public function __set($var, $value)
        {
           
                return $this->$var = $value;
        }

        public function __isset($name)
        {
            echo "Is '$name' set?\n";
            return isset($this->$name);
        }

        public function __invoke($variable)
        {
            var_dump($variable);
        }

        public function __unset($name)
        {
            echo "Unsetting '$name'\n";
            unset($this->$name);
        }

        public function Add_Persoana()
        {
            echo 'am adaugat o persoana';
        }

        public function Delete_Persoana()
        {
            echo 'am sters o perosna';
        }

        function strlen($string)
        {
            echo 'lungimea stringului este: ' . \strlen($string) . "\n";
        }

        private function test()
        {
            echo "this is a test \n";
        }

        public static function __set_state($an_array) // As of PHP 5.1.0
        {
            $obj = new Angajati;
            $obj->name= $an_array['name'];
            $obj->var1 = $an_array['var1'];
            $obj->var2 = $an_array['var2'];
            return $obj;
        }

    }

}